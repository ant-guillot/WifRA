## ui.R ##
library(shiny)
library(shinydashboard)
library(data.table)
library(DT)
library(stargazer)
library(reshape2)
library(ggplot2)
library(plotly)
library(MASS)
library(car)
library(rCharts)
library(RColorBrewer)

header <- dashboardHeader(title = "WIfRA")

sidebar <- dashboardSidebar(
  sidebarMenu(
    menuItem("Data", icon = icon("database"),
             collapsible = 
               menuSubItem('Data importation', tabName = 'ImportBoard'),
             menuSubItem("Data engineering", tabName = "EngineeringBoard"),
             menuSubItem("View data", tabName = "DataBoard")
    ),
    menuItem("Model", icon = icon("cog"), tabName = "ModelRunBoard"),
    menuItem("Summary", tabName = "SummaryBoard", icon = icon("dashboard")),
    menuItem("Plots", icon = icon("line-chart"), tabName = "PlotsBoard"),
    menuItem("Prediction", icon = icon("calendar-plus-o"), tabName = "PredBoard"),
    menuItem("Diagnostics", icon = icon("ambulance"),
             collapsible = 
              menuSubItem('Outliers', tabName = 'OutliersBoard'),
             menuSubItem('Multicollinearity', tabName = 'MulticolBoard'),
             menuSubItem('Normality', tabName = 'NormalityBoard')
    ),
    menuItem("Model comparison", icon = icon("exchange"), tabName = "ComparBoard"),
    menuItem("Info and disclaimer", icon = icon("info-circle"), tabName = "InfoBoard"))
  )


body <- dashboardBody(
  
  
  ###Data Importation UI###
  tabItems(
    tabItem(tabName = "SummaryBoard",fluidRow(box(solidHeader = TRUE,status = "primary",title="SoS Analysis",(dataTableOutput("modelProp"))),
                                              box(solidHeader = TRUE,status = "primary",title="Model",(uiOutput("gen_diag")))),
            box(collapsible = T,width = '60%',solidHeader = TRUE,status = "primary",title="Coeefficients estimate",dataTableOutput("Coeff")),
            box(collapsible = T,solidHeader = TRUE,status = "primary",title="Model performances",width = '60%',dataTableOutput("perf"))),
    tabItem(tabName = "ImportBoard",
            fluidRow(
            
            box(collapsible = T,solidHeader = TRUE,status = "primary",title="Data importation",
            fileInput('Reg_Data', 'Choose CSV File',
                      accept=c('text/csv', 
                               'text/comma-separated-values,text/plain', 
                               '.csv')),
            tags$hr(),
            checkboxInput('header', 'Header', TRUE),
            radioButtons('sep', 'Separator',
                         c(Comma=',',
                           Semicolon=';',
                           Tab='\t'),
                         ','),
            radioButtons('quote', 'Quote',
                         c(None='',
                           'Double Quote'='"',
                           'Single Quote'="'"),
                         '"'),
            radioButtons('rownames', 'Row names',
                         c("Row number"="NULL","First row"="1"))
            ),
            box(collapsible = T,solidHeader = TRUE,status = "primary",title="Data summary",uiOutput("data_summary"))
            ),
            fluidRow(box(collapsible = T,width=12,solidHeader = TRUE,status = "primary",title="Data overview",dataTableOutput("DataView")
            ))
    ),
    ###End of Data Importation UI###
    
    
    ##Data engineering UI##
    tabItem(tabName = "EngineeringBoard",fluidRow(box(title="Data engineering",solidHeader=T,collapsible = T,status = "primary",uiOutput("VartoMod"),uiOutput("DataEngineering")),box(uiOutput("EngineeringHelp")))),
    tabItem(tabName = "DataBoard",column(uiOutput("ViewOptions"),width=3),column(br(),downloadButton('downloadDF', 'Download'),width=3),
            tabBox(
              tabPanel(solidHeader = TRUE,status = "primary","View data table",dataTableOutput("ViewData")),
              tabPanel(solidHeader = TRUE,status = "primary","View boxplot",plotlyOutput("plotData")),width = "100%"
              )

      ),
    ###Running model###
    tabItem(tabName = "ModelRunBoard",
           column(6,
            box(width=12,title="Variable selection",collapsible = T,solidHeader=T,status = "primary",uiOutput("var_selection")),
            box(width=12,title="Segmented regression",collapsible = T,solidHeader=T,status = "primary",
                checkboxInput("Enabled_Seg","Segmented regression?"),
                uiOutput("SegRegression"))

            ),
           column(6,
             box(width=12,title="Interaction selection",solidHeader=T,collapsible = T,status = "primary",uiOutput("inter_selection")),
             box(width=12,title="Regression options",solidHeader=T,status = "primary",
                 actionButton("LaunchReg","Launch regression!"),collapsible = T)),div(uiOutput("ModelSaving"))),
    
   ###Plot tab##
   tabItem(tabName = "PlotsBoard",fluidRow(column(uiOutput("VariableToPlot"),width=4),column(uiOutput("OtherVarLevel"),width=4),column(checkboxInput('ShowPoints','Points',T),checkboxInput('ShowCI','Confidence interval',T),checkboxInput('ShowPI','Prediction interval',T),width=4)),
           box(collapsible = F,title=h4("Model plot",align='center'),solidHeader = TRUE,status = "primary",width="100%",showOutput("MainPlot","highcharts"))
           ),
   
   ###PredictionTab###
   tabItem(tabName = "PredBoard",br(),fluidRow(box(width = 12,collapsible = T,column(6,uiOutput("Prediction_UI")),
           column(2,
                  actionButton("SubmitPred","Predict the value"),
                  br(),br(),
                  downloadButton('downloadPred','DownloadPrediction'),
                  br(),br(),
                  numericInput("AlphaPred","Choose a level of confidence for the prediction",value=0.95, min=0, max=1,step=0.05)))),
           box(width = 12,collapsible = T,title = "Predictions",solidHeader = TRUE,status = "primary",dataTableOutput("Pred_Table"))
           
   ),
   tabItem(tabName = "OutliersBoard",uiOutput("OutlierUI")),
   tabItem(tabName = "NormalityBoard",uiOutput("ResidualsUI")),
   tabItem(tabName = "MulticolBoard",uiOutput("MultiUI")),
   tabItem(tabName ="ComparBoard",fluidRow(br(),
                               column(6,fileInput("imp_mod1",label="Import model 1",accept = "text/csv" ),fileInput("imp_mod2",label="Import model 2",accept = "text/csv" )),
                               column(6,uiOutput("SummaryMod1"),uiOutput("SummaryMod2")),
                               dataTableOutput("AnovaTable"),
                               uiOutput("WarningComparison"))),
   tabItem(tabName ="InfoBoard",uiOutput("Disclaimer"))
   

))



shinyUI(dashboardPage(header, sidebar, body))
