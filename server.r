library(shiny)
library(shinydashboard)
library(data.table)
library(DT)
library(stargazer)
library(reshape2)
library(ggplot2)
library(plotly)
library(MASS)
library(car)
library(lmtest)
library(het.test)
library(rCharts)
library(RColorBrewer)

model_summary<-function(modelIn,correction)
  ###Construction of the table for the summary of the regression
{
  modelBase<-lm(modelIn$model[,1]~1)
  Anova(modelBase,modelIn,type="III")
}

model_perf<-function(modelIn)
{
  sumModel<-summary(modelIn)
  R2<-sumModel$r.squared
  R2adj<-sumModel$adj.r.squared
  AICmod<-AIC(modelIn)
  BICmod<-BIC(modelIn)
  MSE<-mean(modelIn$residuals^2)
  res<-data.frame(R2,R2adj,AICmod,BICmod,MSE)
  colnames(res)<-c("R²","Adj R²","AIC","BIC","MSE")
  res
}
MSE.boot<-function(modelMatrix)
{
  n_obs<-nrow(modelMatrix)
  MSE_vect<-numeric(1000)
  for (i in 1:1000)
  {
  train<-sample(1:n_obs,round(n_obs*0.7))
  test<-(1:n_obs)[-train]
  TrainData<-modelMatrix[train,]
  TestData<-modelMatrix[test,]
  model_train<-lm(TrainData[,1]~.,TrainData[,-1])
  MSE_vect[i]<-mean((predict(model_train,TestData[,-1])-TestData[,1])^2)
  }
  mean(MSE_vect)
 
}


modified_coeff<-function(model,correction)
  ###Construction of the table for the summary of the regression
{
  if (correction)
  {
    sum1<-coeftest(model,vcov=hccm(model))[,]
    DF_mod<-summary(model)$df[2]
    CI<-c(sum1[,1]-qt(0.975,DF_mod)*sum1[,2],sum1[,1]+qt(0.975,DF_mod)*sum1[,2])
  }
  else
  {
    sum1<-summary(model)
    res<-data.frame(sum1$coefficient,sum1$coefficient[,1])
    CI<-confint(model)
  }

  res[,c(2,3)]<-CI
  res<-round(res,2)
  res[,4]<-sum1$coefficients[,3]
  res[,5]<-sum1$coefficients[,4]
  colnames(res)<-c("Estimate","Lower CI bound","Higher CI Bound","t-stat","p-value")

  return(res)
}

lm.beta<-function (MOD,var)
  ###Standardized coefficient
{
  b <- summary(MOD)$coef[var,1]
  sx <- summary(MOD)$coef[var,2]
  sy <- sd(MOD$model[,1])
  beta <- b * sx/sy
  return(beta)
}

###Dummy coding of a factor
Dummy_coding<-function(DF_input)
{
  DF_dummy<-data.frame(DF_input)
  levels_dum<-levels(DF_input)
  for (i in 1:length(levels_dum))
  {
    name_dum<-levels_dum[i]
    DF_dummy[,name_dum]<-NA
    for (j in 1:length(DF_input))
    {
      if (!is.na(DF_dummy[j,1]))
      {DF_dummy[j,name_dum]<-0}
      if (!is.na(DF_dummy[j,1]) & DF_dummy[j,1]==name_dum)
      {DF_dummy[j,name_dum]<-1}
    }
  }
  DF_dummy[,-1]
}

###Effect coding of a dummy
Effect_coding<-function(DF_input)
{
  DF_dummy<-data.frame(DF_input)
  levels_dum<-levels(DF_input)
  for (i in 1:length(levels_dum))
  {
    name_dum<-levels_dum[i]
    DF_dummy[,name_dum]<-NA
    for (j in 1:length(DF_input))
    {
      if (!is.na(DF_dummy[j,1]))
      {DF_dummy[j,name_dum]<-(-1)}
      if (!is.na(DF_dummy[j,1]) & DF_dummy[j,1]==name_dum)
      {DF_dummy[j,name_dum]<-1}
    }
  }
  DF_dummy[,-1]
}

###Test whether or not a variable is a dummy or effect coded variable
is_dumEf<-function(Values)
{
  if (is.factor(Values))
    return(FALSE)
  if (length(levels(as.factor(Values)))==2 & max(Values)==1 & min(Values)==0)
    return(TRUE)
  else
  {
    if (length(levels(as.factor(Values)))==2 & max(Values)==1 & min(Values)==-1)
      return(TRUE)
    else
      return(FALSE)
  }
  
}




server <- function(input, output,session) {
  
  
###Importation of the data###
  values<-reactiveValues()
  values$data<-TRUE
  values$formula<-""
  data_reloaded<-observeEvent(input$Reg_Data,values$data<-data_imported())
  
  ###Imports data with selected options
  data_imported<-reactive({
    if (is.null(input$Reg_Data))
      return(NULL)
    else
    {
      inFile<-input$Reg_Data
      temp<-read.csv(inFile$datapath, header=input$header, sep=input$sep, 
                     quote=input$quote,row.names = eval(parse(text=input$rownames)))
      temp
    }
  })
  
  ###Renders data heads 
  output$DataView<-renderDataTable(
    {
      if (is.null(input$Reg_Data))
        return(data.frame())
      else
      {
        
        data_reg<-head(data_imported())
        return(DT::datatable(data_reg,options = list(paging = FALSE,searching=FALSE)))
      }
    }
  )
  
  ###Quick summary of the data
  output$data_summary<-renderUI({
    if (is.null(input$Reg_Data))
      return(p("Data haven't been loaded yet'"))
    else
    {
      data_reg<-data_imported()
      n_obs<-nrow(data_reg)
      n_var<-ncol(data_reg)
      percent_missing<-mean(is.na(data_reg))*100
      percent_incomplete<-(1-nrow(na.omit(data_reg))/n_obs)*100
      
      return(div(p("The dataframe has", n_obs, "observations and", n_var ,"variables.",
                   br(), round(percent_missing), "% of data are missing.",
                   br(),round(percent_incomplete), "% of obs are incomplete."),
                 br(),
                 div(align="center",withMathJax(),HTML(stargazer(data_reg,type="html",column.sep.width = "30pt")))
                 ))
    }
  })
  
  ###End of data imortation
  
  ####Data engineering###
  ####Reactive fonction to add variable to the dataframe
  
  
  ###Trigger for deletion fo observation and add of new var
  values$Delete<-0
  observeEvent(input$Delete,values$Delete<-input$Delete)
  values$New<-0
  observeEvent(input$CreateVar,values$New<-input$CreateVar)
  
  
  
  data_working<-reactive(
    {
      if (is.null(input$Reg_Data))
      {return(NULL)}
      if (!is.data.frame(values$data))
      {temp<-data_imported()}
      else
      {temp<-values$data}
      if (values$New!=0)
      {
        isolate({
          Var_name<-input$VarToModify
          if (is.factor(temp[,input$VarToModify]))
          {new_value<-switch(input$Operation,
                             "Rename"=temp[,Var_name],
                             "Dummy Coding"=Dummy_coding(temp[,Var_name]),
                             "Effect Coding"=Effect_coding(temp[,Var_name]))
          temp[,gsub(" ", ".",paste(input$NewName,levels(temp[,input$VarToModify])))]<-new_value
          }
          else
          {
            new_value<-switch(input$Operation,
                              "Rename"=temp[,Var_name],
                              "Center"=(temp[,Var_name])-mean(temp[,Var_name],na.rm=T),
                              "Standardize"=temp[,Var_name]/sd(temp[,Var_name],na.rm=T),
                              "Log"=log(temp[,Var_name]),
                              "Exp"=exp(temp[,Var_name]),
                              "^2"=temp[,Var_name]^2,
                              "Sqrt"=abs(temp[,Var_name])^(1/2),
                              "Categorize"=as.factor(temp[,Var_name]),
                              "Custom"=
                                {
                              tempDF<-data.frame(temp[,Var_name])
                              for (i in 1:nrow(tempDF))
                              {if (!is.na(temp[i,Var_name]))
                              {
                                x<-tempDF[i,1]
                                tempDF[i,1]<-eval(parse(text=input$CustomFormula))
                                
                              }
                              }
                              tempDF
                                }
                              
            )
            ###NB:the mean and sd are computed without taking into acccount the NA value
            temp[,input$NewName]<-new_value
            session$sendCustomMessage(type = 'testmessage',
                                      message = 'The new variable has been created')
            
          }
          
        })
      }
      {
        if (values$Delete==0 | is.null(values$Delete))
        {
          values$data<-temp
          return(temp)
        }
        else
        {
          temp<-temp[!row.names(temp) %in% (isolate(input$Todel)),]
          values$data<-temp
          return(temp)
        }
        
      }
    })
  
  ###Data engineering UI
  output$DataEngineering<-renderUI({
    div(
      selectInput("VarToModify",label="Select a variable to modify", choices=colnames(data_working()) ),
      uiOutput("operation_to_app"),
      textInput("NewName",label="Enter the name of the new variable",value="New.variable"),
      actionButton("CreateVar","Create the new var!")
    )
  })
  
  
  ###Data engineering help
  output$EngineeringHelp<-renderUI(
    {
      switch(input$Operation,
             "Rename"=p("Rename the variable"),
             "Dummy Coding"=p("Dummy code the different categories. Only available for factor."),
             "Effect Coding"=p("Effect code the different categories. Only available for factor."),
             "Center"=p("Center the variable, action for na is remove."),
             "Standardize"=p("Standardize the variable to put its variance to 1. NA action is remove"),
             "Log"=p("Logarithm of the variable. Take care of non-positive values !"),
             "Exp"=p("Exponentiate the variable."),
             "^2"=p("Square the variable."),
             "Sqrt"=p("Take the root of the variable, take care of non-positive values !"),
             "Categorize"=p("Recode a variable as factor, you will be able to dummy or effect it then."),
             "Custom"=div(p("Apply a custom formula to the variable. Call the variable 'x' in the expression"),br(),p("Exemple 1:x^2+3*x-log(x)"),br(),p("Exemple 2:{if (x>3) 'Grand' else 'small'} "),textInput("CustomFormula","Type a formula to apply",value="if (x>0) {1} else {x^2}"))
             )
    }
  )

  
  ###Selection of the opertaion to apply on the data
  output$operation_to_app<-renderUI({
    selectInput("Operation",label="Select an operation to apply", choices=Choices_op())
  })
  
  Choices_op<-reactive(
    {
      DF_temp<-isolate(data_working())
      RN<-row.names(DF_temp)
      var_pos<-which(RN == input$VarToModify)
      if (is.factor(DF_temp[,input$VarToModify]))
        c("Rename","Dummy Coding","Effect Coding","Custom")
      else
        c("Rename","Center","Standardize","Log","Exp","^2","Sqrt","Categorize","Custom")
    })
  
  ###End of data engineering###
  
  ###Show data###
  output$ViewOptions<-renderUI({
    selectInput("Var_Displ","Choose variables to display",choices=colnames(data_working()),multiple = T,selected=colnames(data_working())[1])
  })
  
  
  output$ViewData<-DT::renderDataTable(
    {
      if (is.null(input$Reg_Data) & is.null(input$Var_Displ))
        return()
      
      else
      {
        data_reg<-data_working()
        var_to_displ<-unlist(input$Var_Displ)
        data.table(data_reg[,c(var_to_displ)])
      }
    }
  )
  
  ###Boxplot##
  
  output$plotData<-renderPlotly(
    {
      if (is.null(input$Reg_Data) & is.null(input$Var_Displ))
        return()
      
      else
      {
      data_reg<-data_working()
      var_to_displ<-unlist(input$Var_Displ)
      data_temp<-data_reg[,var_to_displ]
      unlist_DF<-data.frame(unlist(data_temp))
      unlist_DF<-data.frame(unlist_DF,rep(unlist(input$Var_Displ),each=nrow(data_temp)))
      colnames(unlist_DF)<-c("Value","Var_name")
      gg<-ggplot(unlist_DF,aes(x=Var_name,y=Value))+geom_boxplot( aes(color=Var_name))+xlab("")+ theme(legend.title=element_blank())+geom_violin(fill='lightblue', alpha=0.5)+geom_jitter(position = position_jitter(width = .1),alpha=0.5)
      
      ggply<-plotly_build(gg)
      pos<-length(ggply[[1]])
      if (!ggply[[1]][[pos-2]]$showlegend)
         {
        ggply[[1]][[pos-2]]$showlegend<-T
        ggply[[1]][[pos-2]]$name<-"hide/show boxplot"
      }
      ggply[[1]][[pos-1]]$name<-"hide/show density"
      ggply[[1]][[pos]]$name<-"hide/show points"
      ggply[[1]][[pos]]$showlegend<-T
      ggply[[1]][[pos-1]]$showlegend<-T
      ggply[[1]][[pos]]$showlegend<-T
      ggply$layout$showlegend<-T
      ggply
      }
    }
  )
  
  ###Downloading data
  output$downloadDF <- downloadHandler(
    filename = function() { paste('data', '.csv', sep="") },
    content = function(file) {
      write.csv(data_working(), file)
      
    })
  
  ###Running model###
  ###Model tab###
  values2<-reactiveValues()
  values2$NewSeg<-0
  observeEvent(input$LaunchReg,values2$NewSeg<-input$LaunchReg)
  
  
  ####Choice of the variable UI###
  output$var_selection <- renderUI({
    
    
    if (is.null(input$Reg_Data))
      return(div(p("Waiting for data")))
    
    else
    {
      inFile<-input$Reg_Data
      data_reg<-data_working()
      n<-length(colnames(data_reg))
      var_name<-colnames(data_reg)
      names(var_name)<-colnames(data_reg)
      interaction_choice<-isolate(interaction_list())
      
      div(
        selectInput("Pred_selection", 
                    label = "Variable to predict", 
                    choices = var_name,
                    selected = 1),
        selectInput("Variable_selection", 
                    label = "Predictors", 
                    choices = var_name,multiple = T,
                    selected = 1))
    }
  })
  
  
  
  
  ###Creating possible interaction####
  
  interaction_list<-reactive({
    if (is.null(input$Reg_Data))
      return(c())
    
    else
    {
      var_name2<-unlist(input$Variable_selection)
      deg_int<-input$DegreeOInt
      res<-c()
      if (length(var_name2)<2)
        return(res)
      else
      {
        for (k in 2:min(deg_int,length(var_name2)))
        {
          inter<-combn(var_name2,m=k,simplify = T)
          for (i in (1:ncol(inter)))
          {
            res<-c(res,paste(paste(inter[,i], collapse=":")))
          }
        }
        res
      }
    }
  })
  
  output$inter_selection <- renderUI({
    
    
    if (is.null(input$Reg_Data))
      return(div(p("")))
    
    else
    {
      interaction_choice<-interaction_list()
      selectInput("VarInteraction", 
                  label = "Choose variable to interact", 
                  choices = interaction_choice, multiple = T,selected = 1)
    }
  })

  ### segmented regression###
  
  
  values2<-reactiveValues()
  values2$NewSeg<-0
  observeEvent(input$LaunchReg,values2$NewSeg<-input$LaunchReg)
  
  ##reactive UI contruction 
  output$SegRegression<-renderUI({
    if (input$Enabled_Seg)
    {
      div(selectInput("BreakVar","Choose the variable to segment",choices=input$Variable_selection,selected = 1),checkboxInput("Jump_en","Enable jump"),numericInput("BreakNumber","Number of break",value=1,min=1,step=1),uiOutput("ManualBreakPoint"))
    }
    else
    {}
  })
  
  output$ManualBreakPoint<-renderUI({
    res<-"div(br()"
    for (i in(1:input$BreakNumber))
    {
      res<-paste(res,"numericInput(",sep=",")
      res<-paste(res,"'","Break",i,"'",",",paste("'","Break",i,"'",",",sep=""),"value=1,min=1,step=1",sep="")
      res<-paste(res,")",sep=" ")
    }
    res<-paste(res,")")
    eval(parse(text=res))
  })
  
  ###Creating break variables###
  CreateBreak<-function(data,var,BreakValue=1,jump=T)
  {
    res<-data.frame(data[,var])
    res$VarBreaked<-res[,1]
    res$VarBreaked[!is.na(res$VarBreaked) & res[,1]<BreakValue]<-0
    res$VarBreaked[!is.na(res$VarBreaked) & res[,1]>=BreakValue]<-res$VarBreaked[!is.na(res$VarBreaked) & res[,1]>=BreakValue]-BreakValue
    if (jump)
    {
      res$Jump<-rep(0,nrow(data))
      res$Jump[!is.na(res$VarBreaked) & res[,1]<BreakValue]<-0
      res$Jump[!is.na(res$VarBreaked) & res[,1]>=BreakValue]<-1
    }
    return(res)
  }
  
  ##Creating the DF dor the segmentation
  
  DataFR2<-reactive(
    {
      if (values2$NewSeg!=0)
      {
        isolate({
          temp<-isolate(data_working())
          for (i in(1:input$BreakNumber))
          {
            breakpos<-input[[paste("Break",i,sep="")]]
            DF_temp<-CreateBreak(temp,input$BreakVar,breakpos,jump=input$Jump_en)
            if (input$Jump_en)
              temp[[paste("Jump",i,sep="")]]<-DF_temp$Jump
            temp[[paste("Break",i,sep="")]]<-DF_temp$VarBreaked
          }
        })
      }
      return(temp)
    })
  
  ###creating model ####
  reg_model<-reactive(
    {
      if (values2$NewSeg!=0)
        isolate({
          data_reg<-isolate(data_working())
          temp_data<-data_reg
          pred_selected<-unlist(input$Pred_selection)
          var_selected<-unlist(input$Variable_selection)
          int_selected<-unlist(input$VarInteraction)
          if (is.null(var_selected) & is.null(int_selected))
            return(NULL)
          else
          {
            IntForm<-""
            VarForm<-""
            SegForm<-""
            if (!is.null(int_selected))
            {
              IntForm<-paste(int_selected, collapse=" + ")
              IntForm<-paste("+",IntForm)
            }
            if (!is.null(var_selected))
            {
              VarForm<-paste(var_selected, collapse=" + ")
            }
            if (input$Enabled_Seg)
            {
              
              temp_data<-cbind(data_reg,DataFR2())
              for (i in 1:input$BreakNumber)
              {
                if (input$Jump_en)
                  SegForm<-paste(SegForm,"+","Jump",i,"+",sep="")
                SegForm<-paste(SegForm,"+","Break",i,sep="")
              }
              
            }
            formula<-formula(paste(paste(pred_selected,"~"),VarForm,IntForm,SegForm))
            values$formula_model<-paste(paste(pred_selected,"~"),VarForm,IntForm,SegForm)

            mod_reg<-lm(formula,temp_data)
            return(mod_reg)
          }
        })
    })
  
  ###Summary tab###
  output$summary_table<-renderUI({
    if (values2$NewSeg==0)
    {}
    else
    {
      mod_temp<-reg_model()
      div(align="center",HTML(stargazer(mod_temp, type="html",ci = T)))
    }
  })
  
  ###Plot Tab####
  
  
  ####Input value
  output$VariableToPlot<-renderUI({
    list_var<-colnames(reg_model()$model)[-1]
    NoDummy<-rep(F,length(list_var))
    NoFactor<-rep(F,length(list_var))
    temp<-isolate(reg_model()$model)
    for (i in 1:length(list_var))
    {
      name_temp<-list_var[i]
      if (!is_dumEf(temp[,name_temp]))
        NoDummy[i]<-T
      if (!is.factor(temp[,name_temp]))
        NoFactor[i]<-T
    }
    div(
      column(6,
             selectInput("InputToPlot","Choose a variable to plot",choices=subset(list_var,NoFactor))),
      column(6,
             selectInput("DummyLineToAdd","Choose the group to display",choices=subset(list_var,!NoFactor),multiple = F))
    )
    
  })
  
  ###Show cotinuous predictors who havent been selected to be plot
  
  ###Liste of quantitative variable
  VariableNeedLvl<-reactive({
    temp<-{
      tempA<-reg_model()$model
      if (input$Enabled_Seg)
        isolate({tempA[,c(2:(ncol(tempA)-input$BreakNumber*(1+input$Jump_en)))]})
      else
        tempA[,-1]
      }
    list_var<-colnames(temp)
    if (is.null(list_var))
      return({c()})
    else
    {

    NoFactor<-rep(F,length(list_var))

    
    for (i in 1:length(list_var))
    {
      if (!is.factor(temp[,i]))
        NoFactor[i]<-T
    }
    temp<-colnames(temp)[-which(colnames(temp) %in% c(input$InputToPlot,subset(list_var,!NoFactor)))]
    }
    
  })
  
  ###UI to input variable
  output$OtherVarLevel<-renderUI({
    if (is.null(input$Variable_selection))
      return(p("No model is loaded."))
    else
    {
      res<-"div(h4('Variables levels')"
      Var_<-VariableNeedLvl()
      
      Var_<-na.omit(Var_)
      if (length(Var_)>0)
      {
        
        for (i in(1:length(Var_)))
        {
          print(i)
          
          if (is.factor(reg_model()$model[,Var_[i]]))
          {
            
          }
          else
          {
            res<-paste(res,"numericInput(",sep=",")
            res<-paste(res,"'",Var_[i],'PL',"'",",",paste("'",Var_[i]," Value","'",",",sep=""),"value=0",sep="")
            res<-paste(res,")",sep=" ")
          }
        }
      }
      else
        return()
      res<-paste(res,")") 
      print(res)

      eval(parse(text=res))
    }
  })
  
  toScatter<-function(DF)
  {
    res<-list()
    for (i in 1:nrow(DF))
      res<-c(res,list(list(x=DF[i,1],y=DF[i,2])))
    return(res)
  }
  toScatter3<-function(DF)
  {
    res<-list()
    for (i in 1:nrow(DF))
      res<-c(res,list(list(DF[i,1],DF[i,2],DF[i,3])))
    return(res)
  }
  
  ###Plot
  MainPlotReac<-reactive({
      ####Variable to plot
    
      x_var<-as.character(unlist(input$InputToPlot))
      y_var_name<-colnames(reg_model()$model)[1]
      y_var<-reg_model()$model[,1]
      Var_<-VariableNeedLvl()
      Var_<-na.omit(Var_)
      ###Variable where value are needed

      
      
      ###Sequence of point to plot the line
      Min_x<-min(data_working()[,x_var],na.rm=T)
      Max_x<-max(data_working()[,x_var],na.rm=T)

      
      ###Construction of the DF for the prediction


      list_var<-colnames(reg_model()$model)[-1]
      n_level<-1
      print(input$DummyLineToAdd)
      if (!(is.null(input$DummyLineToAdd)||input$DummyLineToAdd==""))
      {
        n_level=length(levels(reg_model()$model[[input$DummyLineToAdd]]))
        temp=data.frame(rep(seq(from=Min_x,to=Max_x,length.out = 200),each=n_level),rep(levels(reg_model()$model[[input$DummyLineToAdd]]),200))
        setnames(temp,c(x_var,input$DummyLineToAdd))
      }
      else
      {
      temp<-data.frame(seq(from=Min_x,to=Max_x,length.out = 200))
      setnames(temp,x_var)
      }
      n_obs=200*n_level




      if (length(list_var)>0)
      {
        for (i in(1:length(list_var)))
        {
          ###The current version doesn't handle the factor variable
          if (is.factor(reg_model()$model[,list_var[i]]))
          {
            if (!(list_var[i] %in% input$DummyLineToAdd))
            {
              temp[,list_var[i]]<-rep(levels(temp[,list_var[i]])[1],n_obs)
            }
          }
          ###Values of continuous variable are entered
          else
          {
            if (list_var[i] %in% VariableNeedLvl())
              {temp[,list_var[i]]<-rep(input[[paste(list_var[i],'PL',sep="")]],n_obs)}
          }
        }
      }
      
      if (input$Enabled_Seg)
      {
      for (i in (1:input$BreakNumber))
      {
        if (input$Jump_en)
          temp[,c(paste("Break",i,sep=""),paste("Jump",i,sep=""))]<-CreateBreak(temp,input$BreakVar,input[[paste("Break",i,sep="")]])[,-1]
        else
          temp[,c(paste("Break",i,sep=""))]<-CreateBreak(temp,input$BreakVar,input[[paste("Break",i,sep="")]],F)[,-1]
      }
      }
      
      
      
      


      ###Construction of the prediction
      prediction_temp<-predict(reg_model(),temp)
      Conf_Int_temp<-predict(reg_model(),temp,interval=c("confidence"))
      Pred_Int_temp<-predict(reg_model(),temp,interval=c("prediction"))
      colnames(Pred_Int_temp)<-c("fit2","lwr2","upr2")
      slope_t<-(prediction_temp[length(prediction_temp)]-prediction_temp[2])/(Max_x-Min_x)
      intercept_t<-prediction_temp[1]

      plotDF<-data.frame(temp,Conf_Int_temp,Pred_Int_temp)
      Data_modl<-reg_model()$model


      


      ###Construction of the plot
      colour_list<-c('rgba(128,192,204, .5)','rgba(185,111,111, .5)','rgba(116,185,103, .5)','rgba(122,114,185, .5)','rgba(183,187,141, .5)','rgba(198,205,107, .5)','rgba(228,3,46, .5)','rgba(255,255,99, .5)','rgba(0,155,217, .5)')
      plot<-Highcharts$new()
      if (n_level>1)
      {
      LVL<-levels(Data_modl[,c(input$DummyLineToAdd)])
      for (i in 1:length(LVL))
      {
        k=LVL[i]
        good_scat<-Data_modl[,input$DummyLineToAdd]==k
        good_fit<-plotDF[,input$DummyLineToAdd]==k
        DF_temp=Data_modl[good_scat,c(x_var,y_var_name)]
        
        DF_fit=plotDF[good_fit,c(x_var,'fit')]
        
        DF_area=plotDF[good_fit,c(x_var,'lwr','upr')]
        
        DF_area2=plotDF[good_fit,c(x_var,'lwr2','upr2')]
        
        plot$series(data=toScatter(DF_fit),name=paste('Fitted',LVL[i]),type='line',color=colour_list[i],zIndex = 3)
        if (input$ShowPoints)
        plot$series(data=toScatter(DF_temp),type='scatter',color=colour_list[i],zIndex = 2,linkedTo=':previous',name=LVL[i],marker=list(symbol='circle',radius=6))
        if (input$ShowCI)
        plot$series(data=toScatter3(DF_area), type = 'arearange',
                    fillOpacity = 0.8,
                    lineWidth = 0,
                    zIndex = 1,color=colour_list[i],linkedTo=':previous',name=paste('CI',LVL[i]))
        if (input$ShowPI)
        plot$series(data=toScatter3(DF_area2), type = 'arearange',
                    fillOpacity = 0.6,
                    lineWidth = 0,
                    zIndex = 0,name=paste('PI',LVL[i]),color=colour_list[i],linkedTo=':previous')
      }
      }
      else
      {
        Color_list<-colorRampPalette(brewer.pal(9,'Blues'))(6)
      
        plot$series(data=toScatter(plotDF[,c(x_var,'fit')]),type='line',zIndex = 3,name='Fitted line',color=Color_list[6])
        if (input$ShowPoints)
        plot$series(data=toScatter(Data_modl[,c(x_var,y_var_name)]),type='scatter',zIndex = 2,linkedTo=':previous',marker=list(symbol='circle',radius=6),name='Scatter',color=Color_list[5])
        if (input$ShowCI)
        plot$series(data=toScatter3(plotDF[,c(x_var,'lwr','upr')]), type = 'arearange',
                    fillOpacity = 0.5,
                    lineWidth = 0,
                    zIndex = 1,linkedTo=':previous',name='Confidence Interval',color=Color_list[3])
        if (input$ShowPI)
        plot$series(data=toScatter3(plotDF[,c(x_var,'lwr2','upr2')]), type = 'arearange',
                    fillOpacity = 0.3,
                    lineWidth = 0,
                    zIndex = 0,linkedTo=':previous',name='Prediction Interval',color=Color_list[4])
      }
      plot$xAxis(title=list(text=x_var))
      plot$yAxis(title=list(text=y_var_name))
      plot$tooltip(
        formatter =  "#! function(){return this.series.name + '<br>X: ' + Highcharts.numberFormat(this.point.x,2) + '<br>Y: ' + Highcharts.numberFormat(this.point.y,2) ;} !#"
      )
      plot$addParams(height=500,width='95%')
      plot
      })
        
        






  
  
  
  output$MainPlot<-renderChart2(MainPlotReac())
  
  
  ###Reg summary####
  output$perf<-DT::renderDataTable(
    {
      DT::datatable(model_perf(reg_model()),options = list(paging = FALSE,filtering=F,searching=F))
    }
  )
  
  output$modelProp<-DT::renderDataTable(
    {
      DT::datatable(model_summary(reg_model()),options = list(paging = FALSE,filtering=F,searching=F))
    }
  )

      output$Coeff<-DT::renderDataTable(
        {
          DT::datatable(
            modified_coeff(reg_model(),F),options = list(paging = FALSE,filtering=F,searching=F)
          )
        }
      )
  
  output$gen_diag<-renderUI(
    {
      r2<-round((summary(reg_model())$r.squared)*100,2)
      Fmodel<-round((summary(reg_model())$fstatistic),2)[1]
      DF<-(summary(reg_model())$df)
      p_value<-1-round(pf(Fmodel,DF[1],DF[2]),2)
      isolate({
        if (p_value<0.05)
        {
          p(
            strong(values$formula_model),br(),
            "The model explains",r2,"% of the variance of",strong(unlist(input$Pred_selection)),".",br(),
            "The overall model is significant with",strong("F(",DF[1],",",DF[2],")=",Fmodel,"."), br(),
            "Its p-value is",p_value,".",br())
        }
        else
        {
          p(
            strong(values$formula_model),br(),
            "The model explains",r2,"% of the variance of",strong(unlist(input$Pred_selection)),".",br(),
            "The overall model is not significant with",strong("F(",DF[1],",",DF[2],")=",Fmodel,"."), br(),
            "Its p-value is",p_value,".",br())
        }
      })
    }
  )
  
  ###Predictio Tab####
  
  output$Prediction_UI<-renderUI({
    if (is.null(input$Variable_selection))
      return(p("No model is loaded."))
    else
    {
      res<-"div(br()"
      Var_<-as.character(colnames(reg_model()$model))

      if (input$Enabled_Seg)
      {
        n_var<-length(Var_)-input$BreakNumber[1]*(1+input$Jump_en)
        Var_<-Var_[1:n_var]
      }

      isolate({

      for (i in(2:length(Var_)))
      {

        if (is.factor(data_working()[,Var_[i]]))
        {
          possible_choice<-levels(data_working()[,Var_[i]])
          choice_car<-paste("c(","'",possible_choice[1],"'",sep="")
          for (j in (2:length(possible_choice)))
          {
            choice_car<-paste(choice_car,",","'",possible_choice[j],"'",sep="")
          }
          choice_car<-paste(choice_car,")")
          res<-paste(res,"selectInput(",sep=",")
          res<-paste(res,"'",Var_[i],"'",",",paste("'",Var_[i]," Level","'",",",sep=""),"choices=",choice_car,sep="")
          res<-paste(res,")",sep=" ")
        }
        else
        {
          res<-paste(res,"numericInput(",sep=",")
          res<-paste(res,"'",Var_[i],"'",",",paste("'",Var_[i]," Value","'",",",sep=""),"value=0",sep="")
          res<-paste(res,")",sep=" ")
        }
      }
      })
      res<-paste(res,")")
      eval(parse(text=res))
    }
  })
  
  DF<-reactiveValues()
  DF$A<-data.frame()
  values$SubmitPred<-F
  prediction_tab<-reactive(
    {
      isolate(
        observeEvent(input$Variable_selection,
                           {

                             if (input$Enabled_Seg)
                             {if (length(input$Variable_selection)!=ncol(DF$A)-6-input$BreakNumber[1]*(1+input$Jump_en))
                               DF$A<-data.frame()}
                             else
                             {if (length(input$Variable_selection)!=ncol(DF$A)-6)
                               DF$A<-data.frame()
                             }
                           }
      )
      )
      observeEvent(input$SubmitPred,values$SubmitPred<-input$SubmitPred)
      if (values$SubmitPred)
      {
        isolate({
          pred_tab_temp<-data.frame(LOL=c(1))
          Var_pred<-isolate(input$Variable_selection)
          for (i in (1:length(Var_pred)))
          {
            Var_to_add<-as.character(Var_pred[i])
            pred_tab_temp[,Var_pred[i]]<-isolate(input[[paste(Var_to_add)]])
          }
          if (input$Enabled_Seg)
          {
          for (i in (1:input$BreakNumber))
          {
            if (input$Jump_en)
              pred_tab_temp[,c(paste("Break",i,sep=""),paste("Jump",i,sep=""))]<-CreateBreak(pred_tab_temp,input$BreakVar,input[[paste("Break",i,sep="")]])[,-1]
            else
              pred_tab_temp[,c(paste("Break",i,sep=""))]<-CreateBreak(pred_tab_temp,input$BreakVar,input[[paste("Break",i,sep="")]],F)[,-1]
          }
          }
          alpha_pred<-isolate(input$AlphaPred)
          pred_tab_temp[,"Fitted Values"]<-predict(reg_model(),pred_tab_temp)
          pred_tab_temp[,c("Level of confidence")]<-alpha_pred
          pred_tab_temp[,c("Lower CI","Upper CI")]<-predict(reg_model(),pred_tab_temp,interval="confidence",level=alpha_pred)[,c("lwr","upr")]
          pred_tab_temp[,c("Lower PI","Upper PI")]<-predict(reg_model(),pred_tab_temp,interval="prediction",level=alpha_pred)[,c("lwr","upr")]
          DF$A<-rbind(DF$A,pred_tab_temp[,-1])
          res<-DF$A
          res
          
        })
      }
      
    })
  
  output$Pred_Table<-renderDataTable(prediction_tab())
  
  output$downloadPred <- downloadHandler(
    filename = 'Prediction.csv',
    content = function(file) {
      write.csv(prediction_tab(), file)
      
    })
  
  ###Diagnostic####
    ###Outlier UI####
  
  output$OutlierUI<-renderUI({
    fluidRow(
      box(width=12,column(5,h3("Outlier analysis")),
      column(width=6,br(),selectInput("diagnostic_choosen_outlier", 
                                      label = NULL, 
                                      choices = c("Summary Table","Leverage","Studentized residuals","Influence"),
                                      selected = 1))),
      uiOutput("DiagnosticOutlierUI")
    )
    
  })
  
  ###Diagnostic outlier to show#####
  output$DiagnosticOutlierUI<-renderUI(
    {
      switch(input$diagnostic_choosen_outlier, 
             "Summary Table" = SummaryOutlierUI(),
             "Leverage" = LeverageUI(),
             "Studentized residuals" = StuResUI(),
             "Influence" = InfluenceUI())
    }
  )
  
  ####Outlier Summary table (to implement: delete observation, variable cut-off)#####
  SummaryOutlierUI<-reactive({div(box(width=12,uiOutput("DataDeletion")),box(width=12,dataTableOutput("TableOutlier")))})
  OutlierTab<-reactive({
    #######Observation number
    DataOutlier<-data.frame(row.names=row.names(reg_model()$model),stringsAsFactors = F)
    DataOutlier[,1]<-hatvalues(reg_model())
    DataOutlier[,2]<-"NO"
    DataOutlier[,3]<-cooks.distance(reg_model())
    DataOutlier[,4]<-"NO"
    DataOutlier[,5]<-studres(reg_model())
    DataOutlier[,6]<-"NO"
    DataOutlier[,7]<-"NO"
    colnames(DataOutlier)<-c("Hat-value","High.leverage","Cooks.D","Influential","Internally studentized residuals","High.residual","InAll")
    ###Dealing with cut-off
    ValueCutOffStu<-qt(1-input$CutOffStud,nrow(data_working())-(length(input$Variable_selection)+2))
    for (i in 1:nrow(DataOutlier))
    {
      if (DataOutlier[i,1]>=max(min(input$CutOffHat,CutOffHat_defaut()),input$CutOffHat))
        DataOutlier[i,2]<-c("YES")
      if (abs(DataOutlier[i,5])>=max(min(ValueCutOffStu,CutOffStud_defaut()),ValueCutOffStu))
        DataOutlier[i,6]<-c("YES")
      if (abs(DataOutlier[i,3])>=max(min(input$CutOffCook,CutOffCookD_defaut()),input$CutOffCook))
        DataOutlier[i,4]<-c("YES")
      if (DataOutlier[i,4]=="YES" & DataOutlier[i,6]=="YES" & DataOutlier[i,2]=="YES")
        DataOutlier[i,7]<-"YES"
    }
    DataOutlier
  })
  output$TableOutlier<-renderDataTable(OutlierTab()) 
  
  #####Deleting outlier####
  
  ####Deleting outlier
  output$DataDeletion<-renderUI(
    {
      fluidRow(br(),br(),br(),column(3,offset=3,selectInput("Todel","Choose observation to delete",choices = row_names_data(),multiple=T)),
               column(3,br(),actionButton("Delete","Delete selected observation"))
               
      )
    })
  
  row_names_data<-reactive({
    if (is.null(input$Reg_Data))
      return(c("NO DATA"))
    else
      return(row.names(isolate(data_working())))
  })
  
  
  
  
  
  ####Leverage tab####
  CutOffHat_defaut<-reactive(
    2*(length(input$Variable_selection)+1)/nrow(data_working())
  )
  
  LeverageUI<-reactive({
    fluidRow(box(width=12,
            column(10,plotlyOutput("HatPlot")),
             column(2,numericInput("CutOffHat","Choose a cut-off for the hat-value",value=CutOffHat_defaut(),step=CutOffHat_defaut()/4),br(),p("The advised cut-off is ",strong(round(CutOffHat_defaut(),6)),"."))
                    
            )
             ,br(),
             box(width=12,renderDataTable(subset(OutlierTab(),High.leverage=="YES")))
    )
    
  })
  HatPlotReac<-reactive({
    gg1 <- ggplot(OutlierTab(),aes(y=OutlierTab()[,c("Hat-value")],x=row.names(OutlierTab())))
    gg1 <- gg1 + geom_point()  +
      labs(x="Index",y="Hat value") +
      ggtitle("Hat value")
    gg1<-gg1+geom_hline(aes(yintercept=input$CutOffHat[1]),linetype="dotted",colour="red")
    ggplotly(gg1)
  })
  output$HatPlot<-renderPlotly(HatPlotReac())
  
  ####Internally studentized residual tab####
  CutOffStud_defaut<-reactive(
    qt(0.975,nrow(data_working())-(length(input$Variable_selection)+2))
  )
  StuResUI<-reactive({
    fluidRow(box(width=12,column(10,plotlyOutput("StudResPlot")),
             column(2,numericInput("CutOffStud","Choose a confidence level for the internally studentized residuals",value=0.05,step=0.05/4),
             br(),p("The advised cut-off is ",strong(round(0.05,6)),"."))),br(),
             box(width=12,renderDataTable(subset(OutlierTab(),High.residual=="YES")))
    )
  })
  ResPlotReac<-reactive({
    gg1 <- ggplot(OutlierTab(),aes(y=OutlierTab()[,c("Internally studentized residuals")],x=reg_model()$fitted.values))
    gg1 <- gg1 + geom_point()  +
      labs(x="Fitted value",y="Internally studentized residuals") +
      ggtitle("Studentized residuals vs fitted value")
    ValueCutOffStu<-qt(1-input$CutOffStud,nrow(data_working())-(length(input$Variable_selection)+2))
    gg1<-gg1+geom_hline(aes(yintercept=ValueCutOffStu),linetype="dotted",colour="red")
    gg1<-gg1+geom_hline(aes(yintercept=-ValueCutOffStu),linetype="dotted",colour="red")
    plotly_build(gg1)
  })
  output$StudResPlot<-renderPlotly(ResPlotReac())
  
  ####Cook's Distance tab####
  CutOffCookD_defaut<-reactive(
    isolate(4/length(reg_model()$fitted.values))
  )
  InfluenceUI<-reactive({
    div(
      box(width=12,column(10,plotlyOutput("CookDPlot")),
             column(2,numericInput("CutOffCook","Choose a cut-off for the Cook's distance",value=CutOffCookD_defaut(),step=CutOffCookD_defaut()/4),
             br(),p("The advised cut-off is ",strong(round(CutOffCookD_defaut(),6)),"."))),br(),
             box(width=12,renderDataTable(subset(OutlierTab(),Influential=="YES"))
    ))
  })
  CookPlotReac<-reactive({
    gg1 <- ggplot(OutlierTab(),aes(y=OutlierTab()[,c("Cooks.D")],x=reg_model()$fitted.values))
    gg1 <- gg1 + geom_point()  +
      labs(x="Fitted value",y="Cook's Distance") +
      ggtitle("Cook's Distance vs fitted value")
    gg1<-gg1+geom_hline(aes(yintercept=input$CutOffCook[1]),linetype="dotted",colour="red")
    plotly_build(gg1)
  })
  output$CookDPlot<-renderPlotly(CookPlotReac())
  
  ####Residual analysis UI and fonction#####
  
  ####Residual Normality
  output$ResidualsUI<-renderUI({
    fluidRow(
      h2("Residual analysis",align="center"),br(),br(),
      box(width=12,title="Residual distribution",status="primary",solidHeader = T,br(),br(),
      column(width=6,
             plotlyOutput("ResHist")
      ),
      column(width=6,
             plotlyOutput("ResFitted"))),br(),br(),br(),
      box(width=12,
      strong(h4("Residual autocorelation and heteroscedasticity test",align="center")),br(),
      tableOutput("ResidualDiag2")
      )
    )
  })
  
  ####Plot of the histogram of the residuals
  output$ResHist<-renderPlotly({
    b=(max(reg_model()$residuals)-min(reg_model()$residuals))/20
    gg <- ggplot(reg_model(),aes(x=reg_model()$residuals))
    gg <- gg + geom_histogram(binwidth=b, colour="blue", 
                              aes(y=..density..))
    gg <- gg + stat_function(fun=dnorm,args=list(mean=0,sd=sd(reg_model()$residuals)),size=1,alpha=0.7,colour='blue')
    gg<-gg+ggtitle("Residual distribution")+labs(x="Residual Value",y="Residual density")
    gg<-gg+geom_density(colour='red',size=1,alpha=0.7)
    ggly<-plotly_build(gg)
    
    ggly$data[[1]]$showlegend<-F
    ggly$data[[2]]$showlegend<-T
    ggly$data[[2]]$name<-"Normal"
    ggly$layout$showlegend<-T
    ggly$data[[3]]$showlegend<-T
    ggly$data[[3]]$name<-"Kernel"

    ggly
  })
  
  ####Residual vs fitted hist
  output$ResFitted<-renderPlotly({
    gg <- ggplot(reg_model(),aes(x=reg_model()$fitted.values,y=reg_model()$residuals))
    gg <- gg + geom_point()  +
      labs(x="Fitted Values",y="Residual") +
      ggtitle("Residuals vs Fitted")
    gg<-gg+geom_hline(aes(yintercept=0),linetype="dotted")
    gg<-gg+geom_hline(aes(yintercept=-2*sd(reg_model()$residuals)),linetype="dotted",colour="red")
    gg<-gg+geom_hline(aes(yintercept=2*sd(reg_model()$residuals)),linetype="dotted",colour="red")
    plotly_build(gg)
  })
  
  ###Table for heteroskadicity and autocorrelation
  output$ResidualDiag2<-renderTable({
    DiagTable<-data.frame(Statistic=NA,DF=NA,p=NA,Conclusion=NA,stringsAsFactors = F)
    colnames(DiagTable)<-c("Test statistics value","DoF","p-value","Conclusion")
    
    ###Creating formula
    data_reg<-data_working()
    pred_selected<-unlist(input$Pred_selection)
    var_selected<-unlist(input$Variable_selection)
    formula<-formula(paste(paste(pred_selected,"~"), paste(var_selected, collapse=" + ")))
    
    ###Durbin Watson
    DW_temp<-dwtest(formula,data=data_reg)
    DiagTable["Durbin-Watson",]<-data.frame(DW_temp$statistic,NA,DW_temp$p.value,NA)
    colnames(DiagTable)<-c("Test statistics value","DoF","p-value","Conclusion")
    if (DW_temp$p.value>0.05)
      DiagTable["Durbin-Watson",4]<-"There is no statistical evidence of a first-order autocorrelation"
    else
      DiagTable["Durbin-Watson",4]<-"There is a statistical evidence of a first-order autocorrelation"
    
    ###Breush Pagan
    DW_temp<-bptest(formula,data=data_reg)
    DiagTable["Breush-Pagan",]<-data.frame(DW_temp$statistic,DW_temp$parameter,DW_temp$p.value,NA)
    colnames(DiagTable)<-c("Test statistics value","DoF","p-value","Conclusion")
    if (DW_temp$p.value>0.05)
      DiagTable["Breush-Pagan",4]<-"The assumption of homoskedadicity isn't violated"
    else
      DiagTable["Breush-Pagan",4]<-"There is an evidence of a linear heteroskadicity"
    
    
    DiagTable[-1,]
    
    
    
  })
  
  ################################################################
  ###############P4:MODEL COMPARISON#################################
  #################################################################
  model_loaded<-reactiveValues()
  model_loaded$mod1<-NULL
  model_loaded$mod2<-NULL
  
  
  
  ###Import model1
  mod1_imported<-reactive({
    if (is.null(input$imp_mod1))
      return(NULL)
    else
    {
      inFile<-input$imp_mod1
      readRDS(inFile$datapath)
    }
  })
  
  ####Import model 2
  mod2_imported<-reactive({
    if (is.null(input$imp_mod2))
      return(NULL)
    else
    {
      inFile<-input$imp_mod2
      readRDS(inFile$datapath)
    }
  })
  
  ###Anova table
  
  
  
  output$AnovaTable<-renderDataTable(
    {
      if (is.null(input$imp_mod2) | is.null(input$imp_mod1))
        return(NULL)
      else
      {
        mod1<-mod1_imported()
        mod2<-mod2_imported()
        S1<-summary(mod1)
        S2<-summary(mod2)
        if (nrow(mod1$model)==nrow(mod2$model))
        {
          Res<-data.frame(RSquare=c(0,0),adjRSquare=c(0,0),AIC=c(0,0),BIC=c(0,0))
          Res[,"RSquare"]<-c(summary(mod1_imported())$r.square,summary(mod2_imported())$r.square)
          Res[,"adjRSquare"]<-c(summary(mod1_imported())$adj.r.squared,summary(mod2_imported())$adj.r.squared)
          Res[,"AIC"]<-c(AIC(mod1_imported()),AIC(mod2_imported()))
          Res[,"BIC"]<-c(BIC(mod1_imported()),BIC(mod2_imported()))
          row.names(Res)<-c("Model 1", "Model 2")
          
          DT_diag<-data.frame(Res,ANOVA=c("ANOVA","ANOVA"),anova(mod1,mod2))
          DT::datatable(DT_diag, options = list(paging = FALSE,searching=FALSE))
        }
        else
        {
          obs2<-row.names(mod2$model)
          obs1<-row.names(mod1$model)
          obs<-intersect(obs1,obs2)
          formula1<-formula(paste(paste(colnames(mod1$model)[1],"~"), paste(row.names(S1$coefficients)[-1], collapse=" + ")))
          formula2<-formula(paste(paste(colnames(mod2$model)[1],"~"), paste(row.names(S2$coefficients)[-1], collapse=" + ")))
          mod1<-lm(formula1,data=mod1$model[obs,])
          mod2<-lm(formula2,data=mod2$model[obs,])
          anova(mod1,mod2)
          Res<-data.frame(RSquare=c(0,0),adjRSquare=c(0,0),AIC=c(0,0),BIC=c(0,0))
          Res[,"RSquare"]<-c(summary(mod1)$r.square,summary(mod2)$r.square)
          Res[,"adjRSquare"]<-c(summary(mod1)$adj.r.squared,summary(mod2)$adj.r.squared)
          Res[,"AIC"]<-c(AIC(mod1),AIC(mod2))
          Res[,"BIC"]<-c(BIC(mod1),BIC(mod2))
          row.names(Res)<-c("Model 1", "Model 2")
          DT_diag<-data.frame(Res,ANOVA=c("ANOVA","ANOVA"),anova(mod1,mod2))
          DT::datatable(DT_diag, options = list(paging = FALSE,searching=FALSE))
        }
        
      }})
  
  
  output$WarningComparison<-renderUI(
    {
      if (is.null(input$imp_mod2) | is.null(input$imp_mod1))
        return(NULL)
      else
      {
        mod1<-mod1_imported()
        mod2<-mod2_imported()
        obs2<-row.names(mod2$model)
        obs1<-row.names(mod1$model)
        if (nrow(mod1$model)==nrow(mod2$model))
          div(br(),br())
        else
          div(br(),strong(p("The model haven't been fitted on the same size dataset, the analysis has been done using common observations.")),br(),color="red")
        
      }}
  )
  
  
  output$SummaryMod1<-renderUI({
    if (is.null(input$imp_mod1))
      return(NULL)
    else
    {
      fluidRow(br(),
               p("Predicted variable:",colnames(mod1_imported()$model)[1]),
               p("Predictor:",paste(row.names(summary(mod1_imported())$coefficients)[-1],collapse="/")),
               p("Nb of obs used:",nrow(mod1_imported()$model)))
    }
  })
  output$SummaryMod2<-renderUI({
    if (is.null(input$imp_mod2))
      return(NULL)
    else
    {
      fluidRow(br(),p("Predicted variable:",colnames(mod2_imported()$model)[1]),
               p("Predictor:",paste(row.names(summary(mod2_imported())$coefficients)[-1],collapse="/")),
               p("Nb of obs used:",nrow(mod2_imported()$model)))
    }
  })
  
  ####UI to save model###
  output$ModelSaving<-renderUI({
    if (is.null(reg_model()))
      return(div())
    else
    {
      fluidRow(
        column(6,textInput("ModelName","Enter a name for the model")),
        column(3,br(),actionButton("SaveModel","Save model")),
        textOutput("ListModel")
      )
    }
    
  })
  
  #####Save model
  observeEvent(input$SaveModel,{
    saveRDS(reg_model(), file=paste(input$ModelName,'.RDS'))
  })
  
  #####Multicolinearity diagnostic#####
  #### VIF ######
  VIF_Table<-reactive({
    as.matrix(vif(reg_model()))[,1]
  })
  ####Multicolinearity table
  Mult_Table<-reactive(
    {
      
      Diag_table<-data.frame(VIF=VIF_Table(),Multicolinearity="NO",stringsAsFactors = F)
      for (i in 1:nrow(Diag_table))
      {
        if (Diag_table$VIF[i]>=input$VIF_threshold)
          Diag_table$Multicolinearity[i]<-"YES"
      }
      Diag_table
    }
  )
  output$VIF_Diag<-renderTable(Mult_Table())
  
  ####Multicolinearity UI definiton
  output$MultiUI<-renderUI({
    fluidRow(box(width=12,
      title=h3("Multicolinearity detection",align="center"),status="primary",solidHeader = T,br(),
      column(width=5,offset=0,
             tableOutput("VIF_Diag")
      ),
      column(width=3,offset=0,
             numericInput("VIF_threshold","Choose a threshold for VIF",value=10,min=0)
      ),
      column(width=4,offset=0,
             p("Advised cut-off for VIF are often between 5 and 20. The higher the VIF, the higher the multicolinearity.")
      )
    )
    )
  })
  
  ######
  output$Disclaimer<-renderUI({
    div(
    h2("Informations about the Web interface for regression analysis",align='center'),br(),
    p("I designed this interface as a way to simplify the regression analysis for non-coder. This is my first real RShiny project, and a way to learn SHiny, so don't expect a perfect app :)"),
    
    h3('List of used packages:'),
    tags$ul(
      tags$li("data.table and DT"), 
      tags$li("stargazer"), 
      tags$li("reshape2"),
      tags$li("ggplot2, plotly and rCharts for plot rendering"),
      tags$li("MASS, car, lmtet and het.test for regression analysis")
    ),
    
    h3("Features/improvements i'd like to add"),
    tags$ul(
      tags$li("Interaction analysis, pick a point approach and J&N method, post-hoc analysis"), 
      tags$li("Other types of inferences to correct heteroscadicty: whitening, bootstrap"), 
      tags$li("Ridge, lasso and ElasticNet regression"),
      tags$li("Step-wise, backward and forward model selection"),
      tags$li("Bring all the plotly plots to rCharts"),
      tags$li("Improve code"),
      tags$li("...open to any others idea")
    ),
    
    h3("Bugs"),
    tags$ul(
      tags$li("To find")
    ),
    h3("Link to the project gitlab:"),
    tags$a(href="https://gitlab.com/ant-guillot/WifRA.git", "WifRA git repository")
    
    
    

    
    
    )
    
  })
  
  

}