##  Readme WifRA 

### Goal:

Create an easy way to do regression analysis, to get nice output and plots easily.

### Features:

- One function to create any type of partition chart: D3partitionR() with various options
- Functions to render the output in shiny

### Installation:
On the CRAN